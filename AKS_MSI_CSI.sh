
#to make this instruction work: https://github.com/kubernetes-sigs/azuredisk-csi-driver

RG=k8s
AKS=csi
SUB=12c7e9d6-967e-40c8-8b3e-4659a4ada3ef

#Create the AKS cluster - NOTE! you can't use AZ until https://github.com/kubernetes-sigs/azuredisk-csi-driver/issues/183

az aks create -k 1.15.5  --enable-managed-identity --load-balancer-sku Standard --network-plugin kubenet --network-policy calico -x -c 2 -s Standard_B4ms --nodepool-name base  -g $RG -n $AKS

#assign ownership to the MC_ RG to the MSI
az role assignment create --assignee-object-id `az identity show -g MC_${RG}_${AKS}_westeurope -n ${AKS}-agentpool --query principalId -o tsv` --role "Owner" --scope /subscriptions/${SUB}/resourceGroups/MC_${RG}_${AKS}_westeurope

#go on deploying CSI controller and daemonset
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/azuredisk-csi-driver/master/deploy/crd-csi-driver-registry.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/azuredisk-csi-driver/master/deploy/crd-csi-node-info.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/azuredisk-csi-driver/master/deploy/rbac-csi-azuredisk-controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/azuredisk-csi-driver/master/deploy/csi-azuredisk-controller.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/azuredisk-csi-driver/master/deploy/csi-azuredisk-node.yaml

